--[[
    I'M Jumping 0.3.1 by Klokje
    ================================================================================
 
    Change log:
    0.1:
        - Initial release
        
    0.1.1: 
        - Change name to I'M Jumping

    0.3 
        - Should fixed the bug
 
    0.3.1 
        - Added Jarvan
]]
 
-- Library --------------------------------------------------------------------------
if myHero.charName ~= "LeeSin" and myHero.charName ~= "Jax" and myHero.charName ~= "Katarina" and myHero.charName ~= "JarvanIV" then  return end

require "ImLib"

castedWard = false
casted = false
sp = Spells()
hold = false

function OnLoad()
    PrintChat(" >> I'M Jumping 0.3.1 by Klokje")
    Jumper.GetInstance()
    sp = Spells()
    ConfigBasic = scriptConfig("I'M Jumping", "I'M Jumping") 
    ConfigBasic:addParam("wardjump", "Jump key", SCRIPT_PARAM_ONKEYDOWN, false, GetKey("G")) 
    ConfigBasic:addParam("DrawRange", "Draw Jump range", SCRIPT_PARAM_ONOFF, true)
end

function OnDraw()
    if ConfigBasic.DrawRange then 
        if myHero.charName == "JarvanIV" then
            DrawCircle(myHero.x, myHero.y, myHero.z, 800, ColorARGB.Green:ToARGB())
        else 
            DrawCircle(myHero.x, myHero.y, myHero.z, 625, ColorARGB.Green:ToARGB())
        end 
        
    end
end

function OnTick()
    if not ConfigBasic.wardjump then hold = false return end

    if not hold then 
        hold = true
        
        if myHero.charName == "LeeSin" or myHero.charName == "Jax" or myHero.charName == "Katarina" then 
            if myHero:CanUseSpell(Jumper.ability) == READY and (myHero.charName ~= "LeeSin" or myHero:GetSpellData(Jumper.ability).name == "BlindMonkWOne") then
                local pos = GetDistance(mousePos) <= 600 and mousePos or getMousePos()

                local wards = Minions({name = {"SightWard", "VisionWard"}, team = {myHero.team}, range = 150, source = pos})

                table.sort(wards, function(x,y) return GetDistance(x) < GetDistance(y) end)
                for i, ward in ipairs(wards) do
                    CastSpell(Jumper.ability, ward)
                    return 
                end

                if Jumper.GetWardSlot() == nil then Message.AddMassage("Error: No Wards.", ColorARGB.Red) return end 
                casted = true
                Jumper.PlaceWard(pos.x, pos.z)
            end
        end 

        if myHero.charName == "JarvanIV" and myHero:CanUseSpell(_E) == READY and myHero:CanUseSpell(_Q) == READY and myHero:GetSpellData(_Q).mana + myHero:GetSpellData(_E).mana < myHero.mana then 

            local pos = GetDistance(mousePos) <= 770 and mousePos or getMousePos()
            CastSpell(_E, pos.x, pos.z)
            CastSpell(_Q, pos.x, pos.z)
        end 

    end
end